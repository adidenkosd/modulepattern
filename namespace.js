var App = App || {};

App.define = function (namespace) {
    var parts = namespace.split(".");
    var parent = App;
    var i;

    if (parts[0] == "App") {
        parts = parts.slice(1);
    }

    for (i = 0; i < parts.length; i++) {
        if (typeof parent[parts[i]] == "undefined") {
            parent[parts[i]] = {};
        }

        parent = parent[parts[i]];
    }
    return parent;
};